package com.example.ageapp

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val button1 = findViewById<Button>(R.id.buttonDatePicker)
        button1.setOnClickListener {view->
            clickDatePicker(view)
        }

    }
    fun clickDatePicker(view: View){
        val myCal = Calendar.getInstance()
        val year = myCal.get(Calendar.YEAR)
        val month = myCal.get(Calendar.MONTH)
        val day = myCal.get(Calendar.DAY_OF_MONTH)

        val datePicker = DatePickerDialog(this,
            DatePickerDialog.OnDateSetListener {
                    view, year, month, day ->
                val selectedText = findViewById<TextView>(R.id.selectedDate)
                selectedText.setText(day.toString() + "/" + (month + 1).toString() + "/" + year.toString())
                val sdf = SimpleDateFormat("dd/MM/yyyy")
                val theDate = sdf.parse(day.toString() + "/" + (month + 1).toString() + "/" + year.toString())
                val minutesToSelectedDate = theDate!!.time / 60000
                val minutesToToday = sdf.parse(sdf.format(System.currentTimeMillis())).time / 60000
                val minutesText = findViewById<TextView>(R.id.minutesResult)
                val resultText:String
                if (minutesToToday-minutesToSelectedDate > 0){
                    resultText = (minutesToToday-minutesToSelectedDate).toString()
                }
                else{
                    resultText = "Incorrect date selected"
                }
                minutesText.setText(resultText)
            },
            year,
            month,
            day)
        datePicker.datePicker.maxDate = Date().time - 86400000
        datePicker.show()
    }
    
}